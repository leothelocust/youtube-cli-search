'use strict'

import * as request from 'request'
import * as config from './config'

let key = config.key
let count = 10
let type = process.argv[2]
let q = process.argv[3]

let params = '?key='+key+
             '&part=snippet'+
             '&maxResults='+count+
             '&type='+type+
             '&q='+q

request('https://www.googleapis.com/youtube/v3/search'+params, (error, response, body) => {
    let json = JSON.parse(body)
    let items = json.items

    if (!items || !items.length || items.length == 0) {
        console.log(json)
    } else {
        for (let i = 0; i < items.length; i++) {
            let item = items[i]
            let id = undefined
            if (type == "playlist") {
                id = item.id.playlistId
            } else if (type == "channel") {
                id = item.id.channelId
            } else {
                id = item.id.videoId
            }
            let title = item.snippet.title
            console.log(i+1 + '|' + title + '|' + id)
        }
    }
})
