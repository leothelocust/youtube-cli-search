'use strict'

import * as request from 'request'
import * as config from './config'

let key = config.key
let count = 50
let q = process.argv[2]

let params = '?key='+key+
             '&part=snippet'+
             '&maxResults='+count+
             '&playlistId='+q
let params2 = '?key='+key+
              '&part=snippet'+
              '&maxResults='+count+
              '&playlistId='+q

request('https://www.googleapis.com/youtube/v3/playlistItems'+params, (error, response, body) => {
    let json = JSON.parse(body)
    let items = json.items
    let nextPageToken = json.nextPageToken || undefined

    if (!items || !items.length || items.length == 0) {
        console.log(json)
    } else {
        if (nextPageToken) {
            let options = params2 + '&pageToken=' + nextPageToken
            request('https://www.googleapis.com/youtube/v3/playlistItems'+options, (error, response, body) => {
                let json = JSON.parse(body)
                let allItems = items.concat(json.items)

                return format(allItems)
            })
        } else {
            return format(items)
        }
    }
})

let format = function(items) {
    for (let i = 0; i < items.length; i++) {
        let video = items[i]
        let id = video.snippet.resourceId.videoId
        let title = video.snippet.title
        console.log(i+1 + '|' + title + '|' + id)
    }
}
