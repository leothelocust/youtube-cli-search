PROJECT="YouTube CLI Search"
APP=yt_search.sh

help: ;@echo "COMMANDS"; \
	echo "    global              Install global dependencies"; \
	echo "    setup               Generate a config.ts file (then add your key)"; \
	echo "    install             Install \`npm\` dependencies"; \
	echo "    compile             tsc"; \
	echo "    watchcompile        tsc -w"; \
	echo "    clean               rm build folder";



global:
	@echo "Installing $(PROJECT) Global Dependencies"; \
	npm install -g typescript

setup:
	@echo "Setting up $(PROJECT) Environment"; \
	cp config.example.ts config.ts;

install:
	@echo "Installing $(PROJECT) Dependencies"; \
	npm install

clean:
	rm -rf node_modules

compile:
	tsc

watchcompile:
	tsc -w

.PHONY: help global setup install clean compile watchcompile
