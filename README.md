# Overview

I made this little script so I can play youtube videos locally and have much more 
control over the playback controls. For example, I wanted to be able to play only 
the audio of a music video, and I wanted to have just a video frame overlayed on 
my desktop.

This package was written by me, on arch, and so I cannot guarantee its functionality
on any other operating system.

# Requirements

* MPV (mpv 1:0.23.0-3)

    sudo pacman -Q mpv

* youtube_dl

    sudo pip install youtube_dl

# Installation

First

    npm run all

Put your YouTubeAPI V3 Key config.ts

    make compile

or

    make watchcompile

Run

    ./yt_search.sh
