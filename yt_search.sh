#!/usr/bin/zsh

declare -A OPTIONS
declare type
declare query
declare input
declare times
declare flags
declare playlist

times=0

function search() {
    formatted_query=${query/ /%20}
    result=("${(@f)$(node /home/locust/Projects/devel/yt/search.js $1 $formatted_query)}")
    count=1
    for line in $result; do
        i=$(cut -d'|' -f1 <<< $line)
        title=$(cut -d'|' -f2 <<< $line)
        code=$(cut -d'|' -f3 <<< $line)
        OPTIONS[$i]=$code
        if [ $((i % 2)) -eq 0 ]; then
            echo "\e[32m$i\t$title\e[0m"
        else
            echo "\e[36m$i\t$title\e[0m"
        fi
    done
}
function searchpl() {
    result=("${(@f)$(node /home/locust/Projects/devel/yt/playlist.js $1)}")
    count=1
    for line in $result; do
        i=$(cut -d'|' -f1 <<< $line)
        title=$(cut -d'|' -f2 <<< $line)
        code=$(cut -d'|' -f3 <<< $line)
        OPTIONS[$i]=$code
        if [ $((i % 2)) -eq 0 ]; then
            echo "\e[32m$i\t$title\e[0m"
        else
            echo "\e[36m$i\t$title\e[0m"
        fi
    done
}
function searchch() {
    result=("${(@f)$(node /home/locust/Projects/devel/yt/channel.js $1)}")
    count=1
    for line in $result; do
        i=$(cut -d'|' -f1 <<< $line)
        title=$(cut -d'|' -f2 <<< $line)
        code=$(cut -d'|' -f3 <<< $line)
        OPTIONS[$i]=$code
        if [ $((i % 2)) -eq 0 ]; then
            echo "\e[32m$i\t$title\e[0m"
        else
            echo "\e[36m$i\t$title\e[0m"
        fi
    done
}

function play_video() {
    echo
    case "$input" in
        *-a)
            a=$(cut -d' ' -f1 <<< $input)
            echo "\e[33mAudio Mode..."
            echo
            if [[ -z $flags ]]; then
                mpv --no-video "https://www.youtube.com/embed/${OPTIONS[$a]}"
            else
                mpv --no-video "https://www.youtube.com/playlist?list=$playlist" --ytdl-raw-options=$flags
            fi
            ;;
        *)
            i=$(cut -d'-' -f1 <<< $input)
            echo "\e[33mVideo Mode..."
            echo
            echo "Video moved to scratchpad..."
            echo
            if [[ -z $flags ]]; then
                mpv "https://www.youtube.com/embed/${OPTIONS[$i]}"
            else
                mpv "https://www.youtube.com/playlist?list=$playlist" --ytdl-raw-options=$flags
            fi
            ;;
    esac
}

function search_playlist() {
    echo
    eval search "playlist" $query
    echo
    echo -n "\e[31mThe choice is yours: \e[0m"
    read input
    if [[ -z $input ]]; then
        echo
        playlist
    else
        echo
        playlist=${OPTIONS[$input]}
        eval searchpl ${OPTIONS[$input]}
        echo
        echo -n "\e[31mThe choice is still yours: \e[0m"
        read input
        if [[ -z $input ]]; then
            echo
            search_playlist
        else
            case "$input" in
                [0-9]\-*)
                    i=$(cut -d'-' -f1 <<< $input)
                    flags="playlist-start=$i"
                    echo
                    echo "\e[33mPlaylist Mode..."
                    play_video
                    ;;
                *)
                    echo
                    echo "\e[33mSingle Video Mode..."
                    unset flags
                    play_video
                    ;;
            esac
            search_playlist
        fi
    fi
}
function search_channel() {
    echo
    eval search "channel" $query
    echo
    echo -n "\e[31mThe choice is yours: \e[0m"
    read input
    if [[ -z $input ]]; then
        echo
        channel
    else
        echo
        eval searchch ${OPTIONS[$input]}
        echo
        echo -n "\e[31mThe choice is still yours: \e[0m"
        read input
        if [[ -z $input ]]; then
            echo
            search_channel
        else
            play_video
            search_channel
        fi
    fi
}
function search_video() {
    echo
    eval search "video" $query
    echo
    echo -n "\e[31mThe choice is yours: \e[0m"
    read input
    if [[ -z $input ]]; then
        echo
        video
    else
        play_video
        video
    fi
}


function playlist() {
    echo -n "What playlist do you want? \e[32m"
    read query

    if [[ -z $query ]]; then
        if [ $times -eq 1 ]; then
            times=0
            start
        else
            echo "\e[31m"
            ((times++))
            playlist
        fi
    else
        times=0
        search_playlist
    fi
}
function channel() {
    echo -n "What channel do you want? \e[32m"
    read query

    if [[ -z $query ]]; then
        if [ $times -eq 1 ]; then
            times=0
            start
        else
            echo "\e[31m"
            ((times++))
            channel
        fi
    else
        times=0
        search_channel
    fi
}
function video() {
    echo -n "What video do you want? \e[32m"
    read query

    if [[ -z $query ]]; then
        if [ $times -eq 1 ]; then
            times=0
            start
        else
            echo "\e[31m"
            ((times++))
            video
        fi
    else
        times=0
        search_video
    fi
}

function start() {
    unset flags
    unset playlist
    times=0
    echo
    echo -n "\e[0mSearch by video/playlist/channel? [V/p/c] \e[32m"
    read type
    echo "\e[0m"
    if [[ $type =~ ^[pP]$ ]]; then
        playlist
    elif [[ $type =~ ^[cC]$ ]]; then
        channel
    else
        video
    fi
}

echo "\e[31mYoutube Search"
echo "--------------\e[0m"
start

# read debug
exit 0
