'use strict'

import * as request from 'request'
import * as config from './config'

let key = config.key
let count = 10
let q = process.argv[2]

let params = '?key='+key+
             '&part=snippet'+
             '&order=date'+
             '&type=video'+
             '&maxResults='+count+
             '&channelId='+q

request('https://www.googleapis.com/youtube/v3/search'+params, (error, response, body) => {
    let json = JSON.parse(body)
    let items = json.items

    if (!items || !items.length || items.length == 0) {
        console.log(json)
    } else {
        for (let i = 0; i < items.length; i++) {
            let video = items[i]
            let id = video.id.videoId
            let title = video.snippet.title
            console.log(i+1 + '|' + title + '|' + id)
        }
    }
})
